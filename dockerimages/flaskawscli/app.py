# app.py - a minimal flask api using flask_restful
from flask import Flask
from flask_restful import Resource, Api
import datetime
import subprocess
import json
import math

app = Flask(__name__)
api = Api(app)


class ReportExplorer: 
    def __init__(self):
        self.reports = []

    def getS3status(self):
        s3_buckets = ['aws','s3api', 'list-buckets', '--query', "Buckets[]"]
        returncode = subprocess.check_output(s3_buckets)
        y = json.loads(returncode)
        z = []
        for bucket in y:
            bucket_dict = dict(bucket_name=bucket['Name'], CreationDate=bucket['CreationDate'])
            s3_command = ['aws' ,'s3api', 'list-objects', '--bucket', str(bucket['Name']) ,'--output', 'json', '--query' ,"Contents[]"]
            s3_command_list = json.loads(subprocess.check_output(s3_command))
            try:
                bucket_dict['Total Size in bytes'] = sum([data['Size'] for data in s3_command_list])
            except:
                bucket_dict['Total Size in bytes'] = 0
            try:
                bucket_dict['Total Items'] = len(s3_command_list)
            except:
                bucket_dict['Total Items'] = 0
            try:
                bucket_dict['Last Modified'] = max([data['LastModified'] for data in s3_command_list])
            except:
                bucket_dict['Last Modified'] = 0
            try:
                bucket_dict['Total cost in Doller/GB'] = str((float(bucket_dict['Total Size in bytes'])/1024/1024/1024)*0.023) + " doller"
            except:
                bucket_dict['Total cost'] = "0" + " doller"
            z.append(bucket_dict)
        self.reports.append({'s3-details':z})

class HelloWorld(Resource):
    def get(self):
        reportexplorer = ReportExplorer()
        reportexplorer.getS3status()
        reports = reportexplorer.reports
        return reports

api.add_resource(HelloWorld, '/')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
